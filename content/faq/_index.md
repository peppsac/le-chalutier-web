---
title: "FAQ"
draft: false
---

Mise à jour 04 août 2021

## Pourquoi ce nom "Le Chalutier" ?

Le nom du Chalutier est parti d'une blague entre nous lors de notre fameuse première rencontre fin octobre 2020. Une véritable envie de rassembler, d'aller "à la pêche" pour fédérer et découvrir plein d'autres porteurs de projets !

Le décalage un peu absurde avec la situation géographique nous a bien fait rire et il faut avouer que ce nom interpelle et est plutôt efficace en termes de communication, on le retient facilement et chacun finalement y associe ses propres idées.

Ce nom nous permet aussi de nous amuser autour du champ lexical du Chalutier, on parle ainsi de la Capitainerie, de l'Equipage, des Passagers... et même du Goéland pour un outil de sondage que nous utilisons.

Bon, certes nous avons aussi conscience du côté peu écologique que peut véhiculer l'image d'un chalutier... nous l'envisageons comme un Chalutier "nouvelle génération" donc forcément "vert"... et finalement aujourd'hui ce nom commence à parler à beaucoup de monde, "le Chalu" commence à faire parler de lui et il n'est plus pour le moment question de changer de nom.
<br>

## Quel est le rôle de la mairie de la Baume d'Hostun ?

La municipalité se positionne actuellemment en tant que :

\- facilitatrice aux côtés des porteurs de projets,
<br>- garante de l'ouverture et de l'accessibilité au lieu à tous les habitant·e·s du territoire, tous âges, toute culture confondue,
<br>- relais avec les communes voisines, les différentes collectivités qui maillent notre territoire (agglomération, département, région,... ),
<br>- ... et la suite dépendra de la tournure que prendra le projet !
<br>

## La mairie compte-elle acquérir le lieu ?

Non, elle n'a ni la capacité financière de le faire, ni la "folie" de l'envisager sans un réel projet économiquement viable.
Elle pourrait en revanche être impliquée dans la "transition" selon les modes de financements (Epora etc.).
<br>

## Quid du financement ?

Pour le moment nous sommes dans une phase de recensement des différents modes de financements (du côté des collectivités, des investisseurs, du financement participatif, etc.). Cette question est étroitement liée à la forme juridique et économique que prendra le projet.

Il y a aussi deux types de financements à réfléchir : le financement à l'investissement (pour l'acquisition du lieu, les travaux...) et puis sur certains projets la question du financement du fonctionnement. Même si, idéalement, le modèle économique devra être viable donc sans financement à long termes du fonctionnement. Cela peut quand même être envisagé au début d'une activité le temps que celle-ci se déploie ou en fonction du contenu de l'activité.

Un crowdfunding est à l'étude pour avoir un premier financement rapidement, en attendant l'arrivée des subventions et autres financement sur dossiers qui prennent beaucoup plus de temps.
<br>

## Les propriétaires actuels souhaitent-ils vendrent ou louer le site ?

L'association immobilière, avec qui nous sommes en étroite relation, souhaite vendre le lieu, mais ils ont été touchés par les valeurs de notre projet.

Nous les rencontrons régulièrement pour les tenir au courant des avancées du projet.

Aujourd'hui en août 2021, nous avons signé une convention d'occupation, avec perspective d'achat.
<br>

## Quid du choix des porteurs de projets ?

Au printemps ont eu lieu les ateliers citoyens, des ateliers participatifs ouverts à tous et animés en petits sous-groupes afin d'envisager plusieurs scénarios.

Aujourd'hui en août 2021, nous avons rencontré une trentaine de porteurs de projets. Nous avons organisé des sessions de travail collectives pour connaître les besoins de chacun et commencer une répartition des espaces pour les projets qui veulent s'installer à la rentrée.

Un programme d'activités vous attend donc dès septembre ! Suivez nos actualités pour en savoir plus...
<br>

## Je suis un porteur de projet ou un partenaire potentiel, que dois-je faire ?

Bienvenue à vous ! Plusieurs possibilités s'offrent à vous :

\- Vous êtes porteur d'un projet innovant et recherchez un lieu pour votre activité : vous pouvez ecrire à <a href='mailto:installation@lechalutier.org' target='_blank'>installation@lechalutier.org</a>.
<br>- Vous souhaitez devenir bénévole et participer à la construction du projet : <a href='mailto:benevole@lechalutier.org' target='_blank'>benevole@lechalutier.org</a>.
<br>- Vous souhaitez nous soutenir ? Vous pouvez <a href='https://www.helloasso.com/associations/le-chalutier/adhesions/adhesions-chalutier-2022' target='_blank'>adhérer</a> à l'association. Merci !
<br>Pour toute question, n'hésitez pas à nous contacter par <a href='mailto:contact@lechalutier.org'>e-mail</a> !