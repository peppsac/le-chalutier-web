---
title: "Que s'est-il passé au Chalutier en 2021 ?"
draft: false
---

_Article du 06 déc 2021_

### Vous vous demandez peut-être ce qu'il s'est passé au Chalutier depuis que le collectif a les clés des lieux. 
### Alors ? Plein de choses !

<hr>

## Juillet 2021...<br>
Le premier événement marquant a été la journée Portes-Ouvertes du 11 juillet 2021. Ce jour-là, le Chalutier a ouvert ses portes, pour la première fois, au grand public. Toutes et tous ont pu venir visiter, explorer l'immense bâtiment, déambuler dans le parc et  découvrir le projet.<br>
Nous avons entendu des concerts, dans la chapelle et sur le parvis central, il y avait <a href='https://www.facebook.com/Roul-ta-boule-164515470614643/' target='_blank'>Roul’ ta Boule</a> pour régaler tout le monde. De nombreux bénévoles étaient présents pour l'accueil, la buvette, les visites et les nombreuses questions.
<br>

<p align=center><iframe align=center width="380" height="500" src="../images/photos-ev/retro-2021/PO11072021.png" title="Portes Ouvertes Juillet 2021" frameborder=0></iframe></p>

<hr>

## Août 2021...<br>
Plus tard, le 12 août, le Chalutier a accueilli l'événement départemental <a href='https://www.facebook.com/EnAccords' target='_blank'>_À la fontaine de mon village_</a> : accordéon et violoncelle se sont accordés pour une pause dans la chaleur estivale...

<p align=center><iframe align=center width="380" height="500" src="../images/photos-ev/retro-2021/concert12082021.png" title="Concert Chapelle Août 2021" frameborder=0></iframe></p>

<hr>

## Septembre 2021...<br>
En septembre, le réseau des tiers-lieux de la Drôme, <a href='https://lemoulindigital.fr/cedille-tour/' target='_blank'>Cédille</a>, nous a sollicité pour accueillir une rencontre régionale des tiers-lieux. Ce fut un gros travail pour les bénévoles qui ont dû rendre possible l'accueil de 40 personnes sur deux jours. Cuisiner, ranger, accueillir... quel boulot !<br>
Mais quelle joie ce fut de voir ces visages contents, encourageants et reconnaissants. Cela nous a tous confirmé notre envie de continuer à être une ruche, un lieu de travail et de coopération.

<p align=center><iframe align=center width="600" height="380" src="../images/RTL10-11sept/RTL10-11sept_04.jpg" title="Rassemblement Tiers-lieux 2021" frameborder=0></iframe></p>
<p align=center><iframe align=center width="600" height="480" src="../images/RTL10-11sept/RTL10-11sept_05.jpg" title="Rassemblement Tiers-lieux 2021" frameborder=0></iframe></p>

Le 26 septembre, jour de pluie sur la Drôme... Ouf, le Chalutier c'est immense! Le vide-grenier a donc pu se tenir, au sec, pour le bonheur des chineurs et des vendeurs. De très nombreux stands et des bonnes affaires ce jour-là. 

<p align=center><iframe width="540" height="360" src="https://www.youtube.com/embed/blFhxQ908O4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

<hr>

## Octobre 2021...<br>
Pendant les vacances de la Toussaint, ce sont les ados qui ont peuplé le Chalutier ! <a href='https://lafabriqueturbulente.fr/' target='_blank'>La Fabrique Turbulente</a>, un collectif d'artistes a pris possession du bâtiment B pour une semaine de stage pour les ados. Montage son, prise de vues, théâtre d'ombres, cette semaine créative a énormément plu à celles et ceux qui ont eu la chance d'y participer. Les habitants et curieux ont pu assister à la restitution finale. Quel talent !

<p align=center><iframe align=center width="600" height="380" src="../images/photos-ev/turbulentes2021/01.png" title="Turbulentes 2021" frameborder=0></iframe></p>
<p align=center><iframe align=center width="600" height="380" src="../images/photos-ev/turbulentes2021/IMG_0184.JPG" title="Turbulentes 2021" frameborder=0></iframe></p>

Plus récemment, les familles ont pu venir fêter Halloween. De nombreux enfants se sont déguisés pour vivre des aventures effrayantes, rendues possibles grâce à l'implication d'une petite bande de fêtards.

<p align=center><iframe align=center width="600" height="380" src="../images/photos-ev/halloween2021/09.png" title="Halloween 2021" frameborder=0></iframe></p>
<p align=center><iframe align=center width="380" height="600" src="../images/photos-ev/halloween2021/11.png" title="Halloween 2021" frameborder=0></iframe></p>

<hr>

## Et bien d'autres !<br>

D'autres événements ont suivi : concert et recueil de la mémoire en novembre, et notre Arbre de Noël malheureusement annulé en décembre…
<br>
Un petit clin d’œil quand même de l’équipe qui vous avait préparé des supers déco ;)

<p align=center><iframe align=center width="600" height="380" src="../images/photos-ev/noel_chalu.png" title="Arbre de Noel" frameborder=0></iframe></p>

Nous espérons partager avec vous bien d’autres évènements festifs et joyeux au Chalutier ! À très bientôt...