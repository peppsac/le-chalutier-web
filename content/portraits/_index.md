---
title: "Les Portraits de l'Equipage..."
draft: false
---

Mise à jour 18 nov 2021

Découvrez les fabuleuses et talentueux artisans, créatrices, musiciens... installés au Chalutier !
<br>

## Portrait #1 : Alice Rivière - Céramiste d'Art & Illustratrice

<br>

<p align=center><iframe width="540" height="360" src="https://www.youtube.com/embed/8Srkdw5SQII" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

Alice, romanaise depuis peu, diplômée en illustration et animation depuis 2011, s’est formée à la céramique récemment.<br>
Ayant grandi entourée des sculptures de sa mère, elle a toujours été attirée par le volume, la 3D. Aujourd’hui elle crée des pièces uniques, imbriquant art et utilitaire, en terre cuite (le grès), un matériau solide et pérenne.<br>Et l’illustration dans tout ça ? Elle projette l’édition d’un livre jeunesse.<br>
Les deux pratiques artistiques s’équilibrent. Alors que dessiner lui demande de longs moments de réflexion, travailler la terre lui permet une décharge d’énergie.<br>

<p align=center><iframe width="540" height="360" src="../images/portraits/alice-riviere/vases-petit.jpg" title="Vases Alice Riviere" frameborder="0"></iframe></p>

D’ailleurs, en parlant d’énergie, d’après Alice, celle du Chalutier est <i>« super ! »</i>. Elle qui souhaitait créer sans être isolée, a trouvé un <i>« lieu d’expérimentation, accueillant et toujours ouvert »</i>. C’est en assistant aux réunions du collectif en devenir, qu’elle a eu envie de s’y installer. Aussitôt dit aussitôt fait. Par le biais de l’Artisanoscope, Alice fut la première à prendre possession de son espace.<br>
<i>« J’ai mis un point d’honneur à créer mon cocon comme une vitrine. Si ça peut donner envie à d’autres ... »</i>
<p align=center><i>Bienvenue au Chalutier Alice ! :)</i></p>

Pour la contacter ou la suivre :<br>

<a href='https://www.instagram.com/alice_riviere_' target='_blank'>Instagram @alice_riviere_</a>

<a href='https://www.facebook.com/alice.riviere' target='_blank'>Facebook Alice Rivière</a>

<hr>

## Portrait #2 : Amandine - Bénévole et co-fondatrice du Chalutier

<br>

<p align=center><iframe width="540" height="360" src="https://www.youtube.com/embed/ic-66E1d4tc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

Amandine est une habitante de la Baume d'Hostun bien investie dans son village : conseillère municipale élue référente à l'environnement et maman de deux enfants, elle cherche à changer de mode de vie !<br>
Elle a laissé sa place d'animatrice environnement dans un syndicat de traitement des déchets, pour se consacrer depuis un an à la création du Chalutier, dont elle est co-fondatrice et une des premières rêveuses... Son rêve justement ?

<p><i>"Je rêve de travailler dans un lieu où je m'épanouis, proche de chez moi, où je peux tout autant laisser mes enfants, que faire de la poterie ou du yoga, et même prendre un repas !"</i></p>

Travailler au sein d'une communauté qui partage des envies de mieux-vivre ensemble et de transition sociale, c'est autour de ces valeurs qu'Amandine souhaite voir le projet du Chalutier se développer. Gouvernance, finances, animation, réseaux, ses compétences sont multiples et c'est ainsi qu'aujourd'hui elle est en reconversion dans la stratégie de développement des tiers-lieux.

<p align=center><i>Bonne chance Amandine ! :)</i></p>

<hr>

## Portrait #3 : Faites du Slip - les couturières qui fabriquent des sous-vêtements en tissus de récup !

<br>

<p align=center><iframe width="540" height="360" src="https://www.youtube.com/embed/om2q4NWo6Jg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

Léa, 27 ans et Viridiana, 26 ans, sont couturières et créatrices de sous-vêtements. Les deux jeunes femmes se sont rencontrées en 2015 dans une entreprise de maroquinerie de luxe, où elles ont appris le métier en alternance puis exercé jusqu’en 2019.
<br>
Un soir, autour d’un verre, l’idée émerge : <i>« Et si on faisait un truc ensemble ? On a les mêmes valeurs ! »</i>
Le duo décide alors de se lancer dans le textile afin de proposer une production éco-responsable, et plus particulièrement, dans la création de sous-vêtements. En effet, dans ce secteur la demande est plus importante que l’offre.<br>
Elles choisissent un modèle simple pour femme, créé à base de tissu de récup en coton. De fil en aiguille, elles proposent aujourd’hui également un modèle pour homme, et des accessoires.
<br>
En octobre 2021, Léa et Viridiana rejoignent l’Artisanoscope, et installent leur atelier au sein du Chalutier. Elles se sentent bien dans ce lieu plein d’occupants et d’activités différentes !
<br>

_« On habite ensemble et on bosse ensemble, donc on a besoin de voir d’autres tronches ! :-D »_

<p align=center><i>Soyez les bienvenues au Chalutier les filles !</i></p>

Pour les contacter ou les suivre :<br>

<a href='https://www.instagram.com/faitesduslip' target='_blank'>Instagram @faitesduslip</a>

<a href='https://www.facebook.com/Faites-du-slip-103113061881083/' target='_blank'>Facebook Faites du Slip</a>

<hr>
