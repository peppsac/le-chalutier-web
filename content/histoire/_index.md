---
title: "Un lieu qui porte une histoire et des valeurs fortes"
draft: false
---

## Le domaine de la famille SIMOND

Au début du 20ᵉ siècle vivent dans ce domaine, M. SIMOND, sa deuxième femme Mme SIMOND née De La Roche ainsi que la sœur de celle-ci Mlle Anaïs De La Roche.

Le domaine se compose :

* d’une grande maison de maître
* d’un corps de ferme avec des écuries
* d’un hangar
* d’un beau parc ainsi que de nombreuses terres agricoles ou boisées

À la mort de M. SIMOND, Mme SIMOND, veuve et sans descendance décide de donner sa propriété à une œuvre qui puisse en faire quelque chose d’important.

Un grand soutien est apporté par Mme Phèdre, une amie libraire à Romans qui a de très bonnes connaissances en matière d’économie.

## La donation à la congrégation des filles de la Charité

En 1948 la donation se porte sur les Sœurs de St-Vincent de Paul ou filles de la Charité pour construire une maison de jeunes filles en post-cure de tuberculose pulmonaire non contagieuse. Dès la donation, en février 1949, deux religieuses, Mère Michaud et Sœur Catherine Teste viennent habiter sur place avec Mme Simond et Mlle De La Roche, Mlle Phèdre y séjourne également très régulièrement.

C’est un grand chantier qui commence le 7 février 1949. L’entreprise locale Valette et Mommée emploie beaucoup d’ouvriers de la région. Nombreux sont les habitants du territoire qui participent à la construction de la maison de Cure.

Le corps de ferme est remplacé par l’actuel bâtiment B, avec au rez-de-chaussée des logements pour le personnel, et le hangar est remplacé par le bâtiment principal (bâtiment A), doté à l'époque d’un solarium que nous ne pouvons plus voir aujourd'hui puisque détruit lors des remises aux normes.

## L’inauguration de la maison de Cure

![](../images/le-chalutier/photos/page7.png)

L’inauguration a lieu le 5 juillet 1953 en présence de nombreuses personnalités dont Monseigneur Urtasun, évêque de Valence, Mme Simond, généreuse donatrice, Mère générale des filles de la Charité, le docteur Aujaleu, directeur de l’hygiène sociale au ministère de la Santé publique, M. Reymond, conseiller général et les maires, députés, chefs de cliniques du coin… Les installations suscitent l’admiration de tous et la cérémonie et les festivités sont l’occasion d’une belle fête où tous les habitants du territoire participent… Certains s’en souviennent encore aujourd’hui !

Les sœurs s’entourent de personnel médical compétent dont le docteur Baillot, femme médecin qui assure aussi une partie de la direction et restera plus de 40 ans dans ce lieu.

Le 11 novembre 1963, le préfet de la Drôme, à l’occasion des 10 ans du Centre, remet à Sœur Sébert, supérieure de l’établissement, la croix de Chevalier de la Santé Publique. Là aussi, c’est l’occasion d’une belle fête. À cette époque la maison de cure accueille 60 patientes de 16 à 50 ans.

## Des valeurs qui rayonnent

La tâche d’administration du lieu devenant de plus en plus lourde, la direction n’est plus assurée par les sœurs. Elles en restent les propriétaires (ce qui est toujours le cas actuellement). Elles restent présentes en continuant à travailler sur place et en prenant soin de faire perdurer les valeurs d’origine.

Les sœurs s’investissent dans cette maison de cure mais ne se contentent pas de cela, elles œuvrent aussi sur tout le territoire à travers différentes “missions” :

- une infirmière pour les soins à domicile,
- une assistante sociale,
- une catéchiste qui « monte » régulièrement des pièces de théâtre et de danses souvent par les habitants du territoire qu’elle fait produire auprès des malades…
- elles s’occupent de la paroisse,
- elles connaissent toutes les familles, se rendent chez elles pour des échanges, les soutenir, organiser les célébrations…
- elles organisent des arbres de Noël pour les enfants du village qui y trouvent bien souvent leur plus beau cadeau,
- elles embauchent les nécessiteux, procurent du travail à beaucoup : entre autres pour le ménage, la cuisine, le jardinage, un chauffeur,
- elles s’approvisionnent chez les agriculteurs pour le lait et autres denrées qui ne sont pas produites sur place. Un immense potager permet de nourrir l’ensemble du personnel et des résidents de l’établissement,
- elles jouent aussi un rôle important pour le village, pour de nombreux services comme le transport du courrier à la poste située à Saint-Nazaire via leur chauffeur.

Une nouvelle villa (annexe) est même construite afin d’accueillir une famille locale dont les parents travaillent pour la maison de Cure.

Au fil des années, les terres agricoles, les bois, les biens de la famille Simond sont vendus, loués, donnés afin de permettre aux sœurs de faire perdurer le lieu.

La tuberculose devenant de plus en plus rare, la maison est convertie en maison de convalescence et de repos le 26 mars 1971. Le centre commence à accueillir parmi les patients des hommes puisque pour rappel, le lieu était destiné uniquement aux femmes.

L’association « Bien-être et Santé » en assure la gestion, puis l’Adapt prend le relais en 2016.

En septembre 1991, les trois dernières sœurs présentes, n’ayant pas de relève, rejoignent différentes communautés. Ce départ est célébré et l’ensemble des habitants du territoire tient à participer afin de remercier chaleureusement les sœurs.

## Aujourd’hui…

Des virages et des choix économiques sont pris par la suite, et dès 2006, l’activité du lieu est appelée à être transférée aux abords des hôpitaux pour faciliter l’accès au soin. Il est désormais acté qu’au printemps 2021, ils seront libérés.

J’espère que ce petit historique de ce lieu aura pu vous permettre de cerner un peu mieux le rôle qu’a joué et joue encore actuellement le Centre de Convalescence sur notre Territoire. De nombreuses personnes y travaillent encore, et de plus nombreuses encore ont pu bénéficier d’un moment de répit dans ces lieux. Le calme et la beauté du parc y sont évidemment aussi pour beaucoup. Le lien avec le territoire y est toujours présent, notamment lors des visites des enfants de l’école pour venir chanter auprès des résidents mais aussi par une programmation culturelle organisée par l’Adapt à laquelle peuvent assister les enfants de l’école mais aussi les habitants.

> Nous tenons à remercier Béatrice Layeux, habitante baumoise pour avoir partagé ses précieux souvenirs, carnets, articles de journaux…
