---
title: "Qui trouve-t-on au Chalutier ?"
draft: false
---

Mise à jour 24 janv 2022

Retrouvez ici la liste des activités et services proposés au Chalutier depuis de la rentrée 2021.

<br>

![](../images/logos/logo_artisanoscope_grand.png)
L'Artisanoscope a pour but de valoriser et faciliter le travail des artistes, artisans et artisans d'art par la création d'un collectif et l'ouverture d'un tiers-lieu dédié à l'artisanat.
<br><a href='https://www.lartisanoscope.fr/' target='_blank'>https://www.lartisanoscope.fr/</a>
<br><i> Découvrez les <a href='https://lechalutier.org/portraits' target='_blank'>portraits</a> d'Alice Rivière, céramiste et illustratrice ainsi que Faites du Slip, couturières de l'Artisanoscope installées au Chalutier !</i>
<br>
Et retrouvez la liste des créatrices de l'Artisanoscope installées au Chalutier :<br>
- Alice Rivière, Illustratrice & Céramiste : aliceriviere@hotmail.fr
- Cécile Cheniti, Couturière : cecile.cheniti@orange.fr
- Faitesduslip, Couturières : contactfaitesduslip.contact@gmail.com
- Floriane Jardiné, Tapissière d'ameublement : atelier.floriane.jardine@gmail.com
- Laura Blaskovic, Artiste : laura.blaskovic@gmail.com
<hr>

![](../images/logos/logo_etc2.png)
Le Collectif Etc est composé de quatre architectes, une administratrice ainsi que d’une vingtaine de collaborateurs réguliers.<br>
<i>"Nous souhaitons soutenir des initiatives de changements social et environnemental par l’acte de faire. A travers des expérimentations diverses mobilisant nos compétences en construction bois et métal, conseils, édition, réalisation de films, couture, sérigraphie et autres, en France et en Europe, nous testons l’hypothèse d’une architecture frugale et généreuse. Autant de pistes de sortie à une manière de produire de l’espace énergivore et pyramidale.</i>
<br><a href='http://www.collectifetc.com' target='_blank'>http://www.collectifetc.com</a>
<hr>

![](../images/logos/logo_sakifo_fond.png)
Cette brasserie artisanale est née de l’envie de deux copains, amateurs de bières et de « home-made ».
<br>Sakifo c'est une brasserie associée à un beer-garden : « jardin de bière », le beer-garden est un espace extérieur avec des tables pour se restaurer ou boire un verre. Il est très courant dans les pays d’Europe centrale et du nord de voir ces espaces dans les parcs.
<hr>

![](../images/logos/logo_recyclerie_grand.png)
<i>La Recyclerie du Chalu ne reçoit plus de dons jusqu'à la fin de l'année. Une vente évènementielle sera organisée le samedi 4 décembre au Chalutier. Merci !</i>
<br><a href='https://www.facebook.com/recyclerieduchalu' target='_blank'>https://www.facebook.com/recyclerieduchalu</a>
<hr>

![](../images/logos/logo_hublot.png)
Convivial et productif, le Hublot est un espace de co-working ouvert à tous les entrepreneurs, indépendants, salariés en télétravail, porteurs de projets... qui souhaitent profiter de la dynamique d’un lieu de travail partagé, des bénéfices du travail collaboratif et de la compagnie d’autres personnes.
<br><a href='https://lehublot-cowork.com/' target='_blank'>https://lehublot-cowork.com/</a>
<hr>

![](../images/logos/logo_roultaboule_fond.png)
Clément et Mélanie sont heureux de vous accueillir dans leur caravane vintage pour vous faire passer un agréable moment !
Chaque semaine un thème différent, avec 2 Bagels et 2 sortes de boulettes aux choix.
Toutes les recettes sont fait-maisons à partir de produit frais et locaux.
<br>Retrouvez la caravane de Roul'ta boule lors des évènements du Chalutier...
<br><a href='https://roultaboule.weebly.com/' target='_blank'>https://roultaboule.weebly.com/</a>
<hr>

<!--
![](../images/logos/logo_skaven.jpg)
Micro brasserie artisanale originaire de St Nazaire en Royans.
Fabrication et vente de bières.
<br><a href='https://www.facebook.com/brasserie.skaven/' target='_blank'>https://www.facebook.com/brasserie.skaven/</a>
<br>
<hr>
-->

## Retrouvez également au Chalutier...
<br>
<img src="../images/activites2021_04.png"><br>

Des cours bien-être avec :
- <a href='https://centresamana.wordpress.com/' target='_blank'>Caroline Léothaud</a>
- Coline Moineau (yoga)
- Stéphanie Gelbart (yoga)
- Emilie Cluze (Shiatsu)

Des projets musicaux et artistiques avec :
- Nathanaël Koch (cours de musique)
- Salvatore Mauro (atelier de peinture îcones)
- Alain Meunier (atelier de sculpture)

Venez également prendre des cours d'anglais avec <a href='https://www.smithson.cc/' target='_blank'>Caroline Smithson</a> !

<br>
<hr>

## Vous pouvez aussi nous contacter pour d'autres services :
<br>

## Location de salles

Pour les professionnels et les particuliers (anniversaire, mariage...)

## Evènements réguliers

Concerts, vide-greniers, rencontres à thème...  

## Espace restauration et bar

À venir...

## Un parc arboré

Ouvert à tou·te·s pour vos pique-niques et promenades en famille !