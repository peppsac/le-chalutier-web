---
title: "Le lieu"
draft: false
---

#### Le site

![](../images/le-chalutier/photos/page1.jpg)

Le Centre de convalescence est composé de plusieurs bâtiments d'une surface totale de plancher de 4 000 m² et est situé dans un grand parc arboré d'une superficie de 7,2 hectares.

Retrouvez ci-dessous les bâtiments principaux

#### La villa Saint-Vincent

![](../images/le-chalutier/photos/page2.jpg)

#### Le bâtiment A, entrée principale

![](../images/le-chalutier/photos/page3.jpg)

#### Le bâtiment B

![](../images/le-chalutier/photos/page4.jpg)

#### L'annexe

![](../images/le-chalutier/photos/page5.jpg)

#### Le parc

![](../images/le-chalutier/photos/page6.jpg)


#### Baladez-vous sur le site "comme si vous y étiez"

<div id='map'></div>

<script src='../js/mapbox-gl.js' async></script>

<link href='../css/mapbox-gl.css' rel='stylesheet' async />
<style>
    #map {
        width:100%;
    }

    .mapboxgl-ctrl button {
        margin: 0;
    }

    .mapboxgl-marker.mapboxgl-marker-anchor-center.active {
        filter: invert(1);
    }

    #container {
        margin-bottom: 2em;
    }
    #container:hover {
        cursor: grab;
    }

</style>


Cliquez sur un marqueur sur le plan ci-dessus pour afficher le panorama correspondant.

Vous pouvez déplacer la vue en utilisant la souris.


<div id="container" class="panorama" style="margin-top: 1em; width: 100%;"></div>

<script src='../js/panorama.js' async></script>
