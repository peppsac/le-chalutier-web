---
title: "Rassemblement régional des Tiers-lieux"
draft: false
---

## Retour en images...

Les 10 et 11 septembre derniers nous avons eu le plaisir d'acceuillir sur 2 jours les participants du Rassemblement Régional des Tiers-Lieux !

Nous remercions chaleureusement Marie du réseau <a href='http://www.cedille.pro/' target='_blank'>Cédille</a> qui a co-organisé cet évènement et qui nous a choisi pour l'accueillir :) un grand moment de partage qui nous encourage à poursuivre l'aventure !

Et un grand merci et bravo à tous nos super bénévoles qui ont permis à ce week-end d'être une belle réussite !

![](../images/RTL10-11sept/RTL10-11sept_01.jpg)
![](../images/RTL10-11sept/RTL10-11sept_13.jpg)
![](../images/RTL10-11sept/RTL10-11sept_02.jpg)
![](../images/RTL10-11sept/RTL10-11sept_04.jpg)
![](../images/RTL10-11sept/RTL10-11sept_03.jpg)
![](../images/RTL10-11sept/RTL10-11sept_05.jpg)
![](../images/RTL10-11sept/RTL10-11sept_06.jpg)
![](../images/RTL10-11sept/RTL10-11sept_07.jpg)
![](../images/RTL10-11sept/RTL10-11sept_09.jpg)
![](../images/RTL10-11sept/RTL10-11sept_08.jpg)
![](../images/RTL10-11sept/RTL10-11sept_10.jpg)
![](../images/RTL10-11sept/RTL10-11sept_11.jpg)
![](../images/RTL10-11sept/RTL10-11sept_12.jpg)