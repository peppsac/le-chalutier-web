---
title: "Le Chalu Eclec'tronique !"
image: "images/chaluelec/Affiche_Chalu_elec_V3.jpg"
date: 2022-05-01T11:22:16+06:00
draft: false
---
<h2>Samedi 17 septembre 2022</h2>

Le Chalu Éclec'tronique”, c'est LE RDV à ne pas manquer ! Une loooongue journée éclectique qui offre un panel d'animations variées et de qualité pour le bonheur de tous : visite du patrimoine, marché artisanal, ateliers, conférence, spectacle et concerts sur le site du ChaluTiers-lieu ! Un événement éclectique où nous pourrons nous rassembler histoire de fêter la fin de l'été en beauté et guincher du matin jusqu'au soir, youpiii !

<h2>LE PROGRAMME DÉTAILLÉ :</h2>

<h3>MATINÉE PATRIMOINE de 10H à 12H</h3>
<h4>Visite commentée du site dans le cadre de la Journée Européenne du Patrimoine.</h4>
Cette visite invite à revenir sur la création de cet établissement de soins de « post cure en sanatorium », construit par l’architecte lyonnais Louis Mortamet, à l’initiative de Mme Simond, généreuse habitante de La Baume d’Hostun. Devenu un Centre de soins de suite et de réadaptation, le site a entamé depuis peu une troisième vie, en devenant Tiers-lieu rural et citoyen... Nous vous invitons à découvrir les différentes vies de ce site exceptionnel.

</br>
<h3>APRÈS-MIDI MARCHÉ ET ANIMATIONS à prix libre dès 13H</h3>
<h4>Tout au long de la journée : Ambiance musicale du "Comité Défaite" de Romans et Jeux en bois.</h4>
<ul>
<li><span class="font-weight-bold">13h-18h30 :</span> Marché des artisans avec Ateliers d'initiation à
l'artisanat</li>
<li><span class="font-weight-bold">14h30 :</span> Conférence sur la permaculture</li>
<li><span class="font-weight-bold">16h :</span> Spectacle de clown</li>
<li><span class="font-weight-bold">19h:</span> “La Frappe”, ensemble musical de batucada brésilienne</li>
</ul>

<h3>SOIRÉE CONCERTS ÉLECTRO ACOUSTIQUES dès 20H</h3>
<h4>Venez découvrir l'univers électro acoustique de ces artistes hors pairs...</h4>
<ul>
<li><span class="font-weight-bold">20h :</span> Adama Sound System</li>
<li><span class="font-weight-bold">21h30 :</span> Sumac Dub</li>
<li><span class="font-weight-bold">23h :</span> Compost Collaps</li>
<li><span class="font-weight-bold">00h30 :</span> Trance Gall</li>
</ul>

<h2>Infos pratiques :</h2>
<ul>
<li><span class="font-weight-bold">Visite du patrimoine :</span> 04.75.79.20.86</li>
<li><span class="font-weight-bold">Tarif 4 Concerts :</span> 12€ en pré-vente et 15€ sur place.</li>
</ul>
<h4>Les pré-ventes c'est par ici :</h4>
<a href="https://my.weezevent.com/le-chalu-eclectronique?fbclid=IwAR2fPNQjKnG1VWAZkaWM89Ngh07IfkGsIPqlEq1XtyO4bujRkf1FWuvYyRg">Billetterie Concerts Chalu Eclec'tronique</a> ou via la <a href="https://www.facebook.com/lechalu">Page FB Le ChaluTiers-lieu</a>

</br>
<h4>Bar et restauration sur place à partir de midi :</h4>
Food trucks « Crêpes et Galettes » (salé et sucré),« Magma Terra » (méchoui), « Kiwi Coco » (végan)
</br>
</br>
<h4><a href="https://www.facebook.com/events/472818237654213/">Evènement Facebook</a></h4>
<h4><a href="https://my.weezevent.com/le-chalu-eclectronique?fbclid=IwAR2fPNQjKnG1VWAZkaWM89Ngh07IfkGsIPqlEq1XtyO4bujRkf1FWuvYyRg">Billetterie</a></h4>
