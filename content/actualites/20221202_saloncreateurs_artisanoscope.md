---
title: "Salon des créateurs de l'Artisanoscope"
image: "images/visuelsalondescreateurs web_Recto.jpg"
date: 2022-04-02T11:22:16+06:00
draft: false
---
<h2>Du 2, 3 et 4 décembre 2022</h2>

Réservez votre premier week-end de décembre, car L'Artisanoscope organise un Salon de Créateurs ! Non pas un, ni deux, mais TROIS jours de salon en intérieur au Tiers-lieu du Chalutier, 301 côte Simon à la Baume d'Hostun.

Du 2 au 4 décembre, venez retrouver une trentaine de créateurs qui vous proposent leurs pépites artisanales, locales et uniques ! Une belle occasion de découvrir ou redécouvrir ce lieu pour faire plaisir ou vous faire plaisir avec des cadeaux faits-main et qui font sens.

Des animations et surprises seront aussi au programme.
On vous attend nombreux et nombreuses !

<i>Buvette et petite restauration sur place. </i>

<h2>Infos :</h2>


Entrée gratuite.

Vendredi 2, de 16h à 19h. Vernissage prévu à 18h autour d'un verre.

Samedi 3 et Dimanche 4, de 10h à 19h.