---
title: "Le FORRÓ brésilien s'invite au Chalutier !"
image: "images/forro.jpg"
date: 2022-06-01T11:22:16+06:00
draft: false
---

<h2>Samedi 9 Juillet</h2>

<h3>Mais quezako le “forró” ?</h3>

Le Forró est un style musical originaire du Nord est du Brésil, indissociable de la danse populaire qui embrase dorénavant le Brésil tout entier.

Du côté de la Musique : Le forró c’est un peu la musique du bal populaire brésilien. La musique et le chant sont entraînants et gais, sur un tempo lent ou rapide. On les retrouve surtout au mois de juin lors de la Saint-Jean dans les villages du Brésil.

Du côté de la Danse : Le forró est une danse populaire de couple sensuelle et joyeuse. Les pas sont simples à appréhender et en quelques heures de pratique lors d’un stage il est facile de danser lors d’un bal populaire Brésilien.

Partout en France les cours, les stages et les festivals se multiplient !

Le Chalutier s’y colle pour vous faire découvrir cette nouvelle danse lors de cette journée du samedi 9 juillet consacrée au bal Brésilien en organisant un stage de danse l’après-midi et un concert le soir avec le groupe “MisturaFina” qui est la combinaison de 3 musiciens de 3 continents différents, Afrique, Europe et Amérique du sud. Ce beau mélange a été rendu possible grâce à leur amour pour la musique Forró.

Une belle découverte pour ce début d’été !

<h3> Au programme :  </h3>

 - 16H-19H : Stage de danse Forró avec Carolina Vasconcelos et Pascal (réservation conseillée)
 - 20H30 : Concert de "MisturaFina"

<h4>Possibilité de restauration sur place <i>(réservation conseillée)</i></h4>.
­
<h3> Tarifs et formules:</h3>

- Concert : 6€ (+ adhésion à prix libre pour les non-adhérents à l'asso)
- Concert + Stage : 35€
- Repas + Concert : 20€
- Repas + Concert + Stage de 3h : 45€


<h4> Infos pratiques / réservation : <a href="mailto:evenementiel@lechalutier.org"> evenementiel@lechalutier.org</a></h4>


­
	­
­


<hr>
