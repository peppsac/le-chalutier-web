---
title: "Vide-grenier du Chalu : C'est le moment de chiner !"
image: "images/Vide-grenierDuChalutier_20.11.2022.png"
date: 2022-04-03T11:22:16+06:00
draft: false
---
<h2>Dimanche 20 novembre 2022</h2>

Grrr.... le froid commence à se faire sentir ! C'est le moment de sortir les affaires de mi-saison et petites couvertures, et bien sûr l'occasion de faire du tri… Et de renouveler son stock !
Que ce soit en tant qu'exposant ou visiteur, venir participer au vide-grenier automnal du Chalu, c'est soutenir nos projets futurs ! Au plaisir de vous y retrouver…
 

<i>Buvette et restauration sur place.</i>

<h2>Infos :</h2>
<h3><a href="https://vide-greniers.org/26-Drome/La-Baume-d-Hostun-26/vide-grenier-automnal-du-chalutier_1249753379">vide-greniers.org</a></h3>

<h3><a href="https://www.helloasso.com/associations/le-chalutier/evenements/place-pour-vide-grenier">Inscriptions exposants</a></h3>

Installation en intérieur. 3€ le mètre linéaire. Places limitées !


<h2>Contact :</h2>
<ul>
<li>Mail: evenementiel@lechalutier.org </li>
<li>Tel: 06 38 05 51 68</li>
<ul>
