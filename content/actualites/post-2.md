---
title: " Retours sur…  Chantier participatif, deuxième !!"
image: "images/relook.jpg"
date: 2022-06-02T11:22:16+06:00
draft: false
---

<p>Pour cette deuxième session de relooking des espaces extérieurs, l'association du Chalutier a eu le plaisir d'accueillir <b>10 volontaires de l’association Unis-cité Drôme-Ardèche</b> venus dans le cadre de leur service civique pour <b>prêter main forte aux bénévoles</b> ayant répondu à l'appel. Ces jeunes réalisent ensemble la mission « Tous Dehors » qui favorise le jeu libre et l’imaginaire chez les enfants en milieu scolaire et péri-scolaire. La moitié d'entre eux sont mineurs, ils travaillent sur leur orientation professionnelle depuis le début de leur engagement en novembre 2021 dans le cadre du programme BOOSTER PRO. Et tous ensemble, avec l’appui de leur coordinatrice d’équipe, les volontaires mènent des chantiers chaque semaine chez les partenaires comme nous.<b> Bravo à eux pour leur créativité !</b> Marius, en service civique à la mairie d’Eymeux, s’engage lui pour aider les enfants à faire leurs devoirs et crée de supers outils de com pour la commune. Bénévole au Chalutier, il a donné aussi des bons coups de peinture en cette journée de travail collectif. <b>Merci à tous les participants !</b> </p>

<p>
Côté chantier, les fonds de peinture récoltés ont permis d'offrir <b>un panel de couleurs variées laissant l'inspiration de chacun et chacune s'exprimer.</b> Et le résultat est plutôt bluffant, un grand bravo ! Aussi, c'est dans la joie et la bonne humeur que jeunes et moins jeunes se sont retrouvés autour d'un repas partagé le midi... </p>

<p>
A bientôt pour les prochains chantiers (et oui, y'a encore du boulot) !	­
</p>

<h3>Infos pratiques :<h3>
<p> Si vous souhaiter nous soutenir par le bénévolat, vous pouvez d'ores et déjà communiquer vos disponibilités et envies à la commission logistique par mail : <a href="mailto:logistique@lechalutier.org" target="_blank">logistique@lechalutier.org</a></p>

<p>Le recrutement de jeunes en service civique commence chez Unis-cité : <a href="http://www.uniscite.fr">http://www.uniscite.fr</a></p>

­


<hr>
