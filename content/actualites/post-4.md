---
title: "Un premier Marché en toute intimité."
image: "images/marchearti.jpg"
date: 2022-06-04T11:22:16+06:00
draft: false
---

C’est sous une forte chaleur mais toujours avec la bonne humeur que de nos courageux matelots vous ont accueillis le samedi 21 Mai pour vous faire découvrir les univers des 34 Artisanes et Artisans de cette toute première cuvée du « Marché de Créateurs by L’Artisanoscope ». Et quel cru ! Nombreux d’entre vous nous ont félicité du choix des créatrices et créateurs, ainsi que de la qualité des leurs créations. Tout le mérite leur revient, bien entendu.

Vous n'avez pas pu venir pour cette première ? Y'a pas de lézard car on est heureux d'organiser ces événements ! Nous les organisons pour y faire de joyeux échanges comme de joyeuses rencontres. Nous les organisons pour vous partager notre histoire, nos rêves, nos envies de vivre, travailler et consommer autrement. Et chaque chose en son temps… Forts de vos conseils et de vos retours, nous ne cessons de nous améliorer.

Alors un grand merci au clown Igor, au food truck “Kiwi-coco”, à “Sonj Massage”, merci aux artisans et artisanes de Drôme présents, à L’Artisanoscope, aux bénévoles et à vous qui venez à notre rencontre. Merci de nous accorder votre confiance et nous donner la chance d’exister en tant que tiers-lieu.
Merci pour vos sourires et votre soutien.


Vive l’artisanat et vive le Chalutier !
­
	­
­


<hr>
