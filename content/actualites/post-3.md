---
title: "Quand l'histoire et la musique s'emmêlent..."
image: "images/balademusique.jpg"
date: 2022-06-03T11:22:16+06:00
draft: false
---

Le 15 mai dernier, le public était au rendez-vous pour la balade en musique à la Baume d'Hostun. Après la visite historique du parc et des bâtiments, l’ensemble des cuivres des grands élèves du Conservatoire à rayonnement départemental a résonné dans la Chapelle du site Sainte-Catherine Labouré. Un grand moment musical !



<hr>
