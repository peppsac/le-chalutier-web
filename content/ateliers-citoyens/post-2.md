---
title: "Mardi 11 mai 2021 à Jaillans"
date: 2020-05-11T11:22:16+06:00
image: "images/modele-eco.jpg"
draft: false
---

##  Quel modèle économique ?

Comment trouver un modèle économique adapté pour le projet du Chalutier ?

<a href="https://www.eventbrite.fr/e/billets-atelier-citoyen-le-modele-economique-du-chalutier-152538864851" class="btn btn-main btn-main-sm" target="#blank">Je m'inscris !</a>