---
title: "Mercredi 5 mai 2021 à Hostun"
date: 2020-05-05T11:22:16+06:00
image: "images/activite.jpg"
draft: false
---

## Les activités du Chalutier

Comment les nouvelles activités proposées par Le Chalutier donnent envie d’y faire étape ?

<a href="https://www.eventbrite.fr/e/billets-atelier-citoyens-les-activites-du-chalutier-152315394445" class="btn btn-main btn-main-sm" target="#blank">Je m'inscris !</a>