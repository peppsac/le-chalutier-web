---
title: "Les Ateliers citoyens"
draft: false
---

(dernière màj 03/06/2021)

<p style="color:red">ATTENTION : ANNULATION DU DERNIER ATELIER DU 3 JUIN => REPORTÉ AU 23 JUIN !!</p>

On vous a promis un projet co-construit et à l'écoute de ses habitants, et bien voici l'occasion pour vous de venir participer.

Dans les prochaines semaines vont avoir lieu 5 ateliers participatifs, dont l'objectif est de vous impliquer dans la conception du projet.

De l'imagination du futur lieu à la réflexion sur son modèle économique, il y en a pour tous les goûts !

### Horaires des ateliers : 17h30 à 19h30.
### Détails et inscriptions ci-dessous.