---
title: "Mercredi 23 juin 2021 à Beauregard-Baret"
date: 2020-06-03T11:22:16+06:00
image: "images/travailler.jpg"
draft: false

---

## Travailler ensemble

Comment Le Chalutier répond aux nouvelles manières de travailler ensemble ?

<a href="https://www.eventbrite.fr/e/billets-atelier-citoyen-5-travailler-ensemble-au-chalutier-157926958785" class="btn btn-main btn-main-sm" target="#blank">Je m'inscris !</a>