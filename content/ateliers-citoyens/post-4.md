---
title: "Mardi 25 mai 2021 à Saint-Nazaire-en-Royans"
date: 2020-05-25T11:22:16+06:00
image: "images/intergenerationnel.jpg"
draft: false
---

## Un projet intergénérationnel

Comment permettre au Chalutier de faire une place à chacun de 0 à 100 ans ?

<a href="https://www.eventbrite.fr/e/152545921959" class="btn btn-main btn-main-sm" target="#blank">Je m'inscris !</a>
