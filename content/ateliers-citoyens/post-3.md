---
title: "Jeudi 20 mai 2021 à La Baume d'Hostun"
date: 2020-05-20T11:22:16+06:00
image: "images/parc.jpg"
draft: false
---

## L'utilisation du parc

Quelles nouvelles utilisations du Parc pour renforcer le projet dans son ensemble ?

<a href="https://www.eventbrite.fr/e/billets-atelier-citoyen-le-parc-du-chalutier-152543290087" class="btn btn-main btn-main-sm" target="#blank">Je m'inscris !</a>