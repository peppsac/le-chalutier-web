---
title: "Concert Courant d'Airs"
id: "programmation"
date: 2020-05-02T11:18:16+06:00
image: "images/photos-ev/affiche-2021-CdA.jpg"
draft: false
---

## Samedi 13 novembre 20h

Courant d'Airs, ce sont 13 amateurs complices, passionnés de chansons françaises.<br>
Une chanson française revisitée en duo, en trio, en quartet ou tous en chœur qui balade le public d'une émotion à l'autre.<br>
Le choix des chansons, l'harmonisation et l'accompagnement au piano sont conçus au service des textes, la mise en espace discrète, plongent le spectateur dans l'univers du groupe et ses préoccupations.<br>
Au sein de la compagnie Plein Zef, ces treize-là se sont donnés pour ambition de promouvoir et de faire découvrir des chansons et leurs auteurs pour que vive la chanson française. 
<br>
PAF : 10€, gratuit - 12 ans / Sur réservation uniquement soit à pleinzef@gmail.com, soit au 06 71 39 82 88.<br>
Plus d'infos sur <a href='https://www.courantdairs.net' target='_blank'>www.courantdairs.net</a>.
<br>
<br>
<br>
<br>
<br>
<br>
<br>