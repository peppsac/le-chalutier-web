---
title: "À l'abordage du Chalutier !!"
id: "programmation"
date: 2020-05-10T11:22:16+06:00
image: "images/fete.png"
draft: false
---

## DIMANCHE 11 JUILLET 2021

On est super heureux de vous inviter à nos Portes Ouvertes festives !!

Vous voulez découvrir les bâtiments et le parc ?
Vous êtes curieux de tout connaître sur le projet ?
Venez participer à cette demi-journée et célébrer avec nous toutes les étapes franchies depuis le début de cette aventure !

À partir de 14h : visites guidées animées, stands, ateliers, restauration et buvette sur place, soirée concerts...
 
Pour participer, adhésion à l'association obligatoire et à prix libre, vous pourrez la prendre sur place à l'entrée.

### _On vous attend nombreux !!_
<hr>