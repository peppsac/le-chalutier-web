---
title: "8e Biennale des Arts Singuliers et Innovants"
id: "programmation"
date: 2020-05-01T11:14:16+06:00
image: "images/photos-ev/AfficheBiennale2022.jpg"
draft: false
---

## Du 9 au 24 Avril 2022

<br>La commune de la Baume d'Hostun participe à la 8e biennale des Arts singuliers et innovants qui se déroulera du 9 au 24 Avril 2022.

Pour soutenir la promotion de l’« Art brut », l’« Art singulier » et toutes formes d’« Art innovant » particulièrement communicatifs, le tiers-lieu du Chalutier ouvre ses portes pour l'occasion !

<i>Un vernissage aura lieu sur site le samedi 9 avril à 11h.</i>
 
Et aussi, en prévision le 15 avril à 18h : Une pièce de théâtre "La maison de ma mère", mise en scène de récits de migrations dans une roulotte hippo-mobile.
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<hr>