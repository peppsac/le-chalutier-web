---
title: "Stages ados avec la Fabrique Turbulente !"
id: "programmation"
date: 2020-05-02T11:20:16+06:00
image: "images/photos-ev/fabriqueturbulente_grand.png"
draft: false
---

## Du 25 au 28 octobre 2021
<br>

- Stages de création artistique<br>

- Cinéma, son, théâtre d'ombres<br>

- Ouvert aux 11-17 ans<br>

Par l'association <i>Turbulentes !</i>, en collaboration avec la compagnie <i>Le Théâtre de Nuit</i>.
<br>
Inscriptions en ligne sur <a href='https://www.helloasso.com/associations/turbulentes/evenements/la-fabrique-turbulente-toussaint-2021' target='_blank'>https://www.helloasso.com/associations/turbulentes/evenements/la-fabrique-turbulente-toussaint-2021</a>.
<br>
Plus d'infos sur <a href='https://lafabriqueturbulente.fr' target='_blank'>https://lafabriqueturbulente.fr</a>
<br>
<br>
<br>
<br>
<hr>