---
title: "Concert au Chalutier"
id: "programmation"
date: 2020-05-06T11:22:16+06:00
image: "images/CONCERT_12AOUT.jpg"
draft: false
---

## JEUDI 12 AOÛT 2021

Soyez les bienvenu·e·s au Chalutier pour une soirée toute en musique et poésie.<br>
Buvette et restauration sur place.<br>
Info Pass sanitaire : toutes les mesures sanitaires seront respectées de manière citoyenne, merci.<br>
Plus d'infos sur <a href='https://www.facebook.com/EnAccords' target='_blank'>Facebook En Accords</a>.
<br>
<br>
<br>
<br>
<hr>