---
title: "Halloween pour les petits !"
id: "programmation"
date: 2020-05-02T11:21:16+06:00
image: "images/photos-ev/Affiche_Halloween2021.jpg"
draft: false
---

## Samedi 30 octobre

De 14h à 18h, tout un programme pour les enfants : chasse aux trésors, calèche, goûter musical, et plein d'autres surprises !
<br>
Préparez vos plus beaux déguisements !!
<br>
Nous recherchons des bénévoles disponibles pour l'organisation, si ça vous intéresse merci de vous inscrire : 
<a href='https://docs.google.com/spreadsheets/d/1wSZXVLl1PEPSc9vtmWDPEidZbA4hmDbQ2x0O_0LkD5s/edit#gid=0' target='_blank'>Tableau inscription bénévoles</a>
<br>
<i>Evènement co-organisé par le Chalutier et la Team Vercors (Point Info Jeunesse de La Baume).</i>
<br>
<br>
<br>
<hr>