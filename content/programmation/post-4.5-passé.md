---
title: "L'Arbre de Noël du Chalutier + concert"
id: "programmation"
date: 2020-05-01T11:22:16+06:00
image: "images/photos-ev/Annule-arbre-noel-2021.jpg"
draft: false
---

## ANNULÉ - Samedi 4 Décembre

<br>Bonjour à tous, 

Nous avons du prendre la décision d’annuler l'Arbre de Noël ce Samedi 4 Décembre à cause du contexte sanitaire et de plusieurs cas covid au sein de l'équipe du Chalutier.

Toute l’équipe s’était mobilisée pour vous offrir une superbe journée de Noël, une organisation aux petits oignons s’était mise en place et de nombreuses personnes avaient participé de près ou de loin à la création de cette journée. Nous les remercions chaleureusement :)

Sachez que ce n'est que partie remise, et toutes les belles décos préparées seront prêtes pour Noël prochain ! ;)

Haut les cœurs, en espérant vous retrouver très bientôt dans de meilleures conditions !

Merci de votre compréhension,

_L'équipe du Chalutier_
<br>
<br>
<br>