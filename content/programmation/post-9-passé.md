---
title: "<i><Evènements passés...><br></i>Soirée 100% Cubaine !!"
id: "programmation"
date: 2020-05-01T11:15:16+06:00
image: "images/photos-ev/Affiche-soiree cubaine-12mars.jpg"
draft: false
---

## Samedi 12 Mars 2022

<br>Le Chalutier vous propose une Soirée 100% Cubaine à la Baume d'Hostun !!
<br>Avec le groupe « Cubania del Son » : un événement musical et dansant haut en couleurs pour annoncer le printemps et la reprise des festivités au Chalutier !
<br>Standards ou compositions, l'orchestre d'Adiel Castillo Aguilera vous transportera par ses rythmes et mélodies comme si vous étiez au Buena Vista Social Club sur l'ile de Cuba ! Embarquement immédiat pour une soirée dansante sans modération.
<br><u><i>PROGRAMME :</i></u>

- 19H Ouverture des festivités : Apéro et grignotis

- 20H30  Concert : "Cubania del Son"

<i>Restauration et bar sur place</i>

<u><i>TARIFS :</i></u>

- PAF CONCERT "Cubania del Son" : 8€ (adhésion Chalutier comprise)

- REPAS CALIENTE : 12€ (Formule Plat + Dessert Cubain + 1 boisson offerte)

<u><i>RESERVATION :</i></u>

- Billeterie en ligne : <a href='https://www.helloasso.com/associations/le-chalutier/evenements/soiree-cubaine-12-03' target='_blank'><i>cliquez ici !</i></a>

- Billeterie sur place : paiement par chèque et espèces uniquement (nombre de places limitées)

- Par téléphone au 06 38 05 51 68 ou 07 84 28 41 64

<i>Réservation conseillée !</i>
<br>