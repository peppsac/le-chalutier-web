---
title: "Vente Recyclerie du Chalu"
id: "programmation"
date: 2020-05-01T11:20:16+06:00
image: "images/photos-ev/Vente-recyclerie-14janv2022.png"
draft: false
---

## Vendredi 14 janvier 2022

<br>Pour bien démarrer l'année, venez consommer durable en profitant de la vente organisée par la <a href='https://www.facebook.com/recyclerieduchalu' target='_blank'>Recyclerie du Chalu</a> !
<br>

#### Ouverture de la boutique de 16h à 20h au Chalutier le vendredi 14 janvier.<br>

#### _Vente uniquement ! (pas d'accueil de dons)_<br>

Si vous vous sentez concerné·e par la présence d'une recyclerie sur le territoire, une réunion publique aura lieu le _mercredi 26 janvier à 14h_ au Chalutier pour aborder ce sujet et l'avenir de la recyclerie du Chalu.<br>Cette réunion est ouverte à tou·te·s, n'hésitez pas à venir participer !
<br>
Et pour en savoir plus, suivez nos actualités sur <a href='https://www.facebook.com/lechalu' target='_blank'>Facebook</a> et <a href='https://2741e05d.sibforms.com/serve/MUIEAD6ogvOn-vjiTatL1tZ2vdxKHiPcB8S1lqLa14HqvNsMhk9hKKlmwfzXKwYYgrFjKqsbSFMbNnTxS5s7_yxZRrmSt7g17e4hBBv81nBytuVBLbbLuu84IgB0HQYHqlU7XdlhpKrcYw2KaOCKKmSq7l8qalFvpV5I8gy4W18oLDDed2PiJEPJI62SDfOAQMgTCVw1fWTnNl3k' target='_blank'>abonnez-vous</a> à la newsletter du Chalutier !<br>
<br>
<br>
<br>
<br>
<br>