---
title: "Recueil de Mémoires..."
id: "programmation"
date: 2020-05-02T11:19:16+06:00
image: "images/photos-ev/Affiche-long-recueil2021-petit.jpg"
draft: false
---

## Samedi 13 novembre 14h - 19h

Comme nous l'évoquons souvent, le site du Chalutier a une histoire bien à lui, une histoire ancrée dans notre territoire... alors afin de la (re)transmettre (et aussi parce que nous sommes curieux !), nous aimerions la connaître dans ses moindres détails et pour cela, nous avons besoin de vous !<br>
Vous qui avez travaillé ici, qui y avez vécu des temps forts de vos vies, qui avez rendu visite à une personne qui vous était chère...
<br>
Aussi rendez-vous samedi 13 novembre entre 14h et 19h pour une première _"collecte de souvenirs, recueil de la parole"_.
<br>
Toutes les formes d'expressions seront les bienvenues : écrire, raconter, dessiner, montrer... nous serons là pour vous accueillir et vous écouter.
<br>
N'hésitez pas à en parler autour de vous, notamment auprès des personnes que vous savez concernées par ce sujet.
<br>
<br>
<br>
<br>
<br>