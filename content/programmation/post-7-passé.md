---
title: "L'Artisanoscope à l'honneur !"
id: "programmation"
date: 2020-05-01T11:17:16+06:00
image: "images/photos-ev/AfficheInaugurationArti25Mars22-2.jpg"
draft: false
---

## Vendredi 25 mars 2022

<br>

### Inauguration des ateliers et Rencontre avec les artisans

L'Artisanoscope, collectif d'artisans drômois et co-créateur de votre tiers-lieu <i>Le Chalutier</i>, est heureux de vous inviter le <i>vendredi 25 Mars à partir de 17h</i>, à l'inauguration des ateliers et nouveaux locaux. L'occasion d'être ensemble et de fêter la fin des travaux !

Venez à la rencontre des 11 artisans qui ont prit place à bord. Venez découvrir également les futures salles de cours, d'où les artisanes et artisans vous proposerons stages et formations.

<i>Osez L'Artisanoscope, rendez-vous le 25 Mars !</i>
 
Et aussi : "Le Marché des Créateurs de l'Artisanoscope" prévu le 21 mai prochain...
 
Plus d'infos :

https://www.lartisanoscope.fr

https://www.facebook.com/Lartisanoscope
<br>