---
title: "Un Cluedo grandeur nature"
id: "programmation"
date: 2020-05-01T11:16:16+06:00
image: "images/photos-ev/AfficheCluedo18Mars2022.jpg"
draft: false
---

## Vendredi 18 Mars 2022 à 20h

<br>Dans le cadre de la Fête des Laboureurs de la Baume d'Hostun et en partenariat avec le Chalutier, l'association Familles Rurales de Jaillans organise un Cluedo géant sur le site Sainte-Catherine Labouré qui offre moult cachettes et... secrets !
<br>En famille ou entre amis, venez suivre l'enquête en direct live !

<i>Participation/Adhésion au Chalutier : 1€</i>
 
Plus d'infos :

kidoludo.jaillans@gmail.com

04 75 48 86 54

www.famillesrurales.org/jaillans
<br>
<br>
<br>
<br>
<br>
<br>
<br>