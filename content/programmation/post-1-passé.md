---
title: "Le Chalutier fait son chantier !!"
id: "programmation"
date: 2020-05-11T11:22:16+06:00
image: "images/chantier_naval.png"
draft: false
---

##  MERCREDI 30 JUIN 2021

On appelle tous les équipiers disponibles pour participer au premier chantier de notre navire, afin de le rendre tout beau avant son ouverture au public cet été !

RDV mercredi 30 juin à partir de 10h :
#### - pour une journée de ménage et bricolage
#### - activités intérieures et extérieures selon météo et chaleur
#### - le tout dans une ambiance de rencontre, partage et convivialité !
#### - repas partagé le midi et apéro partagé le soir...

_Contactez-nous par mail si vous souhaitez participer !_
<br>
<br>
<hr>