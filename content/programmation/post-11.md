---
title: "Buvette, tapas, concert et ciné le 3 juin au Chalu !"
id: "programmation"
image: "images/photos-ev/projection_plein_air_affiche_03.06.2022_compress.jpg"
draft: false
---

## Le 3 Juin 2022 à partir de 20H

Le partenariat entre la FOL Drôme Ardèche et le Comité des fêtes de La Baume d'Hostun déjà bien ancré dans la commune lors des multiples projections ayant eu lieu dans la salle des fêtes, s'élargit pour cette session en intégrant l'association du ChaluTiers-lieu.
<br/>En effet, le site Sainte-Catherine Labouré offre un espace plutôt propice au cinéma en plein air ! Le film projeté sera "Le Temps des secrets", écrit et réalisé par Christophe Barratier, sorti en 2022. Il s'agit d'une adaptation du roman du même titre, troisième tome des souvenirs d'enfance de Marcel Pagnol paru en 1960.<br/>
Un concert acoustique d'Ambraze, groupe local du Chalutier, ouvrira les festivités à partir de 20h. Aussi, afin d'agrémenter la soirée, un petit service de restauration sera proposé : buvette et tapas provençales, petites mises en bouche juste avant de plonger dans l'univers de Pagnol...<br/>
Infos pratiques :

Amenez chaises, couverture et autres pour vous installer confortablement !

Au programme :

De 19h à 20h : Visite guidée d'une heure du ChaluTiers-lieu.

À partir de 20h : Buvette, tapas et concert acoustique du groupe local « Ambraze » (Prix libre).

À 22h : Projection du film « Le Temps des secrets » (Tarif plein 6,50€ / Tarif réduit 5,50€ / Tarif groupe 4,50€ / Scolaire 4€).
<hr>
