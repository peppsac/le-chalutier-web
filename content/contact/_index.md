---
title: "Contact"
contact:
  title: "Autres contacts :"
  content: "<b>Infos pratiques : </b><br/><br/>
    Si vous souhaitez être informés des prochains chantiers et/ou participer en tant que bénévole, pour l'entretien du parc ou du potager, n'hésitez pas à venir nous rencontrer sur place ou nous contacter par mail : evenementiel@lechalutier.org.
    <br/><br/>
    Et surtout, vos disponibilités seront les nôtres, venez quand vous pouvez !<br/>
    Aussi, nous manquons de matériel divers et varié pour l'entretetien des  bâtiments et du parc, pour l'aménagement du potager, si vous avez de quoi nous dépanner par quelque don que ce soit, prenez contact avec la commission logistique :
    logistique@lechalutier.org"
  emailRecrutement : "recrutement@lechalutier.org"
  emailLogistique : "logistique@lechalutier.org"
  emailEvenementiel : "evenementiel@lechalutier.org"
  emailGestion : "gestion@lechalutier.org"

office:
  title : "Pour nous contacter :"
  # mobile : "0124857985320"
  emailContact : "contact@lechalutier.org"
  location : "301 Côte Simon, 26730 La Baume-d'Hostun"
  locationlink : "https://www.openstreetmap.org/#map=16/45.0573/5.2122"
  # gpslink: "45.0573,5.2122"
  facebook : Tiers-lieu le Chalutier
  facebooklink : "https://www.facebook.com/lechalu"
  insta : tierslieuchalutier
  instalink : "https://www.instagram.com/tierslieuchalutier/"
  content : ""
  linkedin : Le Chalutier
  linkedinlink : "https://www.linkedin.com/company/le-chaluttier/"
  youtube : Chaîne Youtube Tiers-lieu Chalutier
  youtubelink : "https://www.youtube.com/channel/UCZyrPq0kKnxTtbF5pF0vDuQ"

image: "images/carte.png"

# opennig hour
# opennig_hour:
#   title : "Opening Hours"
#   day_time:
#     - "Monday: 9:00 – 19:00"
#     - "Tuesday: 9:00 – 19:00"
#     - "Wednesday: 9:00 – 19:00"
#     - "Thursday: 9:00 – 19:00"
#     - "Friday: 9:00 – 19:00"
#     - "Saturday: 9:00 – 19:00"
#     - "sunday: 9:00 – 19:00"

draft: false
---
