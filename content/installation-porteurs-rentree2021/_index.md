---
title: "Ça se remplit au Chalutier !"
draft: false
---

## Retour en images...

Depuis la rentrée, les allées et venues bâtent leur plein au Chalutier, qui voit doucement s'installer ses nouveaux occupants...

_La Recyclerie du Chalu_ a fait appel à ses bénévoles pour aménager les garages en espaces de stockage, et a d'ores et déjà, après un pot d'accueil convivial, commencé son activité de collecte des dons !
<a href='https://lechalutier.org/activites/' target='_blank'>Plus d'infos.</a><br>
_Alice Rivière (Artisanoscope)_ a repeint et aménagé une salle en adorable atelier de céramique et illustration, tout comme _Faites du slip_ qui est en train d'installer son atelier de couture.<br>
Enfin une belle _salle de yoga_ est en cours de préparation !
<br>
<br>
La Recyclerie du Chalu
![](../images/installation-porteurs-rentree2021/IMG_8558.jpg)
![](../images/installation-porteurs-rentree2021/IMG_8564.jpg)
![](../images/installation-porteurs-rentree2021/IMG_8569.jpg)
![](../images/installation-porteurs-rentree2021/IMG_8627.jpg)
![](../images/installation-porteurs-rentree2021/pot-accueil-recyclerie01.jpg)
![](../images/installation-porteurs-rentree2021/accueil_recyclerie01.jpg)
![](../images/installation-porteurs-rentree2021/tri-recyclerie01.jpg)
<br>
L'atelier d'Alice
![](../images/installation-porteurs-rentree2021/IMG_8629.jpg)
![](../images/installation-porteurs-rentree2021/IMG_8638.jpg)
<br>
La Faites du Slip !
![](../images/installation-porteurs-rentree2021/faites-du-slip.png)
<br>
La salle de yoga
![](../images/installation-porteurs-rentree2021/IMG_8640.jpg)
![](../images/installation-porteurs-rentree2021/IMG_8643.jpg)